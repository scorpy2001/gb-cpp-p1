﻿#include <iostream>

class IA
{
public:
    IA()
    {
        std::cout << "IA constructor" << std::endl;
    }

    virtual ~IA()
    {
        std::cout << "IA destructor" << std::endl;
    }

    virtual void ShowInfo() = 0;
};

void IA::ShowInfo()
{
    std::cout << "Default implementation for IA." << std::endl;
}

class B : virtual public IA
{
public:
    B()
    {
        std::cout << "B constructor" << std::endl;
    }
    ~B()
    {
        std::cout << "B destructor" << std::endl;
    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
        std::cout << "ShowInfo in class B." << std::endl;
    }

};

class C : virtual public IA
{
public:
    C()
    {
        std::cout << "C constructor" << std::endl;
    }
    ~C()
    {
        std::cout << "C destructor" << std::endl;
    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
        std::cout << "ShowInfo in class C." << std::endl;
    }

};

class D : virtual public B, virtual public C
{
public:
    D()
    {
        std::cout << "D constructor" << std::endl;
    }
    ~D()
    {
        std::cout << "D destructor" << std::endl;
    }

    virtual void ShowInfo() override
    {
        IA::ShowInfo();
        B::ShowInfo();
        C::ShowInfo();
        std::cout << "ShowInfo in class D." << std::endl;
    }
};

int main()
{
    D* a = new D;
    a->ShowInfo();

    std::cout << "Before delete" << std::endl;
    delete a;
    std::cout << "After delete" << std::endl;

    return 0;
}
